FROM ubuntu:18.04
MAINTAINER Goncharov Sergey 'serg_yarovoye@ngs.ru'

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Novosibirsk
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get clean && apt-get update -y && apt-get install -y locales
RUN locale-gen en_US.UTF-8

RUN apt-get install -y python-pip python-dev build-essential python-virtualenv python3.6-dev libpq-dev

RUN adduser money_transfer
WORKDIR /home/money_transfer

COPY requirements.txt requirements.txt
RUN virtualenv --python=python3.6 venv
RUN venv/bin/pip install -r requirements.txt

COPY app app
COPY migrations migrations
COPY money_transfer.py boot.sh config_logger.cfg config.cfg db_create.sql ./
RUN chmod +x boot.sh
RUN chown -R money_transfer:money_transfer ./


USER money_transfer
EXPOSE 5020
ENTRYPOINT ["./boot.sh"]
