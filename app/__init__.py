import logging
from flask_restplus import Api
from flask import Blueprint
from flask_cors import CORS

from .main.controller.user_controller import api as auth_ns
from .main.controller.transfer_controller import api as transfer_ns

logger = logging.getLogger(__name__)

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='API Transfer money',
          version='1.0',
          description='`Requires header "pwt"` (token) for transfer methods'
          )
CORS(blueprint, max_age=3600)

api.add_namespace(auth_ns)
api.add_namespace(transfer_ns)
