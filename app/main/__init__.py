import logging
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from .config import Config

logger = logging.getLogger(__name__)

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)
    db.init_app(app)

    return app
