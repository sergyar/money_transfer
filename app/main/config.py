import os
import sys
from configparser import ConfigParser

basedir = os.path.abspath(os.path.dirname(__file__))


def get_config(path):
    if os.path.exists(path):
        cfg = ConfigParser()
        cfg.read(path)
    else:
        print("Config not found! Exiting!")
        sys.exit(1)

    return cfg


cfg = get_config('config.cfg')


class Config():
    DEBUG = False
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'database.db')
    SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{pw}@{url}/{db}'.format(user=cfg['postgresql']['user'],
                                                                                    pw=cfg['postgresql']['password'],
                                                                                    url=cfg['postgresql']['url'],
                                                                                    db=cfg['postgresql']['db'])

    SQLALCHEMY_TRACK_MODIFICATIONS = False
