import logging
from http import HTTPStatus

from flask import request
from flask_restplus import Resource

from app.main.service.user_service import token_required, get_one as get_user
from app.main.service.transfer_service import get_all, create
from ..util.dto import TransferDto

logger = logging.getLogger(__name__)

api = TransferDto.api
new_transfer_dto = TransferDto.new_transfer
transfer_dto = TransferDto.transfer


@api.route('/create')
@api.response(HTTPStatus.BAD_REQUEST, 'Impossible transfer or sender/receiver not found')
class TransferCreate(Resource):
    @token_required
    @api.expect(new_transfer_dto, validate=True)
    def post(self):
        """Creates new transfer from current user"""
        token = request.headers.get('pwt')
        user = get_user(token=token)
        return create(user, request.json)


@api.route('/list')
@api.response(HTTPStatus.NOT_FOUND, 'User not found')
class TransferList(Resource):
    @token_required
    @api.marshal_list_with(transfer_dto, envelope='items')
    def post(self):
        """Gets current user's transfers (outgoing & incoming)"""
        token = request.headers.get('pwt')
        user = get_user(token=token)
        return get_all(user)
