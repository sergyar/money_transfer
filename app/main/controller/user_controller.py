import logging
from http import HTTPStatus

from flask import request
from flask_restplus import Resource

from ..service.user_service import login, create
from ..util.dto import UserDto

logger = logging.getLogger(__name__)

api = UserDto.api
user_login_dto = UserDto.user_login
new_user_dto = UserDto.new_user
user_token_dto = UserDto.user_token


@api.route('/create')
@api.response(HTTPStatus.CONFLICT, 'Email already exists')
class UserCreate(Resource):
    @api.expect(new_user_dto, validate=True)
    def post(self):
        """Creates new user"""
        data = request.json
        return create(data)


@api.route('/login')
@api.response(HTTPStatus.NOT_FOUND, 'User not registered')
@api.response(HTTPStatus.BAD_REQUEST, 'Email or password incorrect')
class UserLogin(Resource):
    @api.expect(user_login_dto, validate=True)
    @api.marshal_with(user_token_dto)
    def post(self):
        """Login"""
        data = request.json
        return login(data)
