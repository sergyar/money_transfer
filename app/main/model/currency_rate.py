from app.main.model.currency import Currency
from .. import db


class CurrencyRate(db.Model):
    __tablename__ = "currency_rate"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    from_currency_id = db.Column(db.Integer, db.ForeignKey('currency.id'), nullable=False)
    to_currency_id = db.Column(db.Integer, db.ForeignKey('currency.id'), nullable=False)
    rate = db.Column(db.Numeric(10, 2), nullable=False)
