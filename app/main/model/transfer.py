from .. import db


class Transfer(db.Model):
    __tablename__ = "transfer"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date = db.Column(db.DateTime, nullable=False)
    sender_user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    sender_currency_id = db.Column(db.Integer, db.ForeignKey('currency.id'), nullable=False)
    sender_amount = db.Column(db.Numeric(10, 2), nullable=False)
    receiver_user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    receiver_currency_id = db.Column(db.Integer, db.ForeignKey('currency.id'), nullable=False)
    receiver_amount = db.Column(db.Numeric(10, 2), nullable=False)
