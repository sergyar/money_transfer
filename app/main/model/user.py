from .. import db


class User(db.Model):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    token = db.Column(db.String(50), unique=True)
    last_login = db.Column(db.DateTime)
    currency_id = db.Column(db.Integer, db.ForeignKey('currency.id'), nullable=False)
    balance = db.Column(db.Numeric(10, 2), nullable=False)
