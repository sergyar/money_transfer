# coding=utf-8
import logging
import requests
import sys

logger = logging.getLogger(__name__)


API_URL = 'https://api.exchangeratesapi.io'
API_METHOD = API_URL + '/latest?base={currency_code}'
TIME_OUT = 10


def get_rates(currency_code_from, valid_currency_codes=None):
    """
     Get data from Currency API     
     :param currency_code_from:
     :param valid_currency_codes:
     :return: list of dict {currency_code_to, rate}
    """
    result = None
    url = API_METHOD.format(currency_code=currency_code_from)
    data = get_http_result(url)
    if data:
        result = []
        try:
            rates = data['rates']
            for code in rates.keys():
                if not valid_currency_codes or code in valid_currency_codes:
                    result.append({
                        'currency_code_to': code,
                        'rate': rates[code]
                    })
        except KeyError as e:
            logger.error('Can`t find in token json: {}, user: {}'.format(e, user))

    return result


def get_http_result(url):
    result = None
    try:
        r = None
        try:
            r = requests.get(url, timeout=TIME_OUT)
            r.raise_for_status()
            result = r.json()
        except (requests.ConnectionError, requests.HTTPError, ValueError) as e:
            logger.error(f'{sys._getframe().f_code.co_name} {type(e).__name__}: {e}, url: {url} response: {r}')

    except Exception as e:
        logger.error(f'{sys._getframe().f_code.co_name} {type(e).__name__}: {e}, url: {url}')

    return result
