import logging
import sys

from app.main.model.currency import Currency
from app.main.model.currency_rate import CurrencyRate
from .api_currency.currency import get_rates as api_get_rates
from app.main import db

logger = logging.getLogger(__name__)

# берем только эти валюты
VALID_CURRENCY_CODES = ['EUR', 'USD', 'GBP', 'RUB', 'BTC']


def create(name):
    currency = get_one(name=name)
    if currency:
        return currency

    currency = Currency(
        name=name
    )
    try:
        db_save(currency)
    except Exception as e:
        logger.error(f'{sys._getframe().f_code.co_name} {type(e).__name__}: {e}')

    return currency


def convert(from_currency_id, to_currency_id, amount):
    """
    Конвертирует сумму между валютами
    :param from_currency_id:
    :param to_currency_id:
    :param amount: amount to convert
    :return: converted_amount
    """
    if from_currency_id == to_currency_id:
        return amount

    rate_info = CurrencyRate.query\
        .filter_by(from_currency_id=from_currency_id)\
        .filter_by(to_currency_id=to_currency_id)\
        .first()

    if not rate_info:
        logger.error(f'{sys._getframe().f_code.co_name} не найден курс: {from_currency_id} -> {to_currency_id}')
        return None

    return amount * rate_info.rate


def refresh_rates():
    """
    Обновляет курсы валют в БД
    """
    for code in VALID_CURRENCY_CODES:
        new_rates = api_get_rates(code, VALID_CURRENCY_CODES)
        if new_rates:
            currency_from = get_one(name=code)
            if not currency_from:
                currency_from = create(code)

            for new_rate in new_rates:
                currency_to = get_one(name=new_rate['currency_code_to'])
                if not currency_to:
                    currency_to = create(new_rate['currency_code_to'])
                rate_info = CurrencyRate.query \
                    .filter_by(from_currency_id=currency_from.id) \
                    .filter_by(to_currency_id=currency_to.id) \
                    .first()
                if not rate_info:
                    rate_info = CurrencyRate(
                        from_currency_id=currency_from.id,
                        to_currency_id=currency_to.id,
                        rate=new_rate['rate']
                    )
                else:
                    rate_info.rate = new_rate['rate']

                try:
                    db_save(rate_info)
                except Exception as e:
                    logger.error(f'{sys._getframe().f_code.co_name} {type(e).__name__}: {e}')


def get_one(id=None, name=None):
    if id:
        return Currency.query.filter_by(id=id).first()
    if name:
        return Currency.query.filter_by(name=name).first()
    return None


def db_save(data):
    db.session.add(data)
    db.session.commit()
