import logging
import sys
from datetime import datetime
from http import HTTPStatus

from app.main import db
from app.main.model.transfer import Transfer
from app.main.util.responses import response_fail, response_ok
from app.main.service.user_service import get_one as get_user
from app.main.service.currency_service import convert as convert_currency

logger = logging.getLogger(__name__)

# минимальная сумма для перевода
MIN_AMOUNT = 0.01


def create(sender, data):
    if not sender:
        return response_fail(HTTPStatus.BAD_REQUEST, 'Sender user not found')

    receiver = get_user(id=data['receiver_user_id'])
    if not receiver:
        return response_fail(HTTPStatus.BAD_REQUEST, 'Receiver user not found')

    if sender.id == receiver.id:
        return response_fail(HTTPStatus.BAD_REQUEST, 'Impossible transfer')

    sender_amount = data['amount']
    if sender_amount < MIN_AMOUNT:
        return response_fail(HTTPStatus.BAD_REQUEST, 'Transfer amount is too low')

    if sender.balance < sender_amount:
        return response_fail(HTTPStatus.BAD_REQUEST, 'Sender balance is too low')

    receiver_amount = convert_currency(sender.currency_id, receiver.currency_id, sender_amount)
    if not receiver_amount:
        return response_fail(HTTPStatus.INTERNAL_SERVER_ERROR, 'Internal server error')

    sender.balance = sender.balance - sender_amount
    receiver.balance = receiver.balance + receiver_amount
    transfer = Transfer(
        date=datetime.utcnow(),
        sender_user_id=sender.id,
        sender_currency_id=sender.currency_id,
        sender_amount=sender_amount,
        receiver_user_id=receiver.id,
        receiver_currency_id=receiver.currency_id,
        receiver_amount=receiver_amount
    )
    try:
        db_save(sender, receiver, transfer)
    except Exception as e:
        logger.error(f'{sys._getframe().f_code.co_name} {type(e).__name__}: {e}')
        return response_fail(HTTPStatus.INTERNAL_SERVER_ERROR, 'Internal server error')

    return response_ok('Successfully created')


def get_all(user):
    if not user:
        return response_fail(HTTPStatus.NOT_FOUND, 'User not found')

    result = []

    outgoing = Transfer.query.filter_by(sender_user_id=user.id).all()
    if outgoing:
        result.extend(outgoing)

    incoming = Transfer.query.filter_by(receiver_user_id=user.id).all()
    if incoming:
        result.extend(incoming)

    return result


def db_save(sender, receiver, transfer):
    db.session.add(sender)
    db.session.add(receiver)
    db.session.add(transfer)
    db.session.commit()
