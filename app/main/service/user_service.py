import logging
import sys
import uuid
from datetime import datetime, timedelta
from functools import wraps
from http import HTTPStatus

from flask import request
from flask_restplus import abort
from sqlalchemy import func

from app.main import db
from app.main.model.user import User

from app.main.util.responses import response_fail, response_ok

logger = logging.getLogger(__name__)

# период авторизации в часах
AUTH_PERIOD = 24


def login(data):
    user = get_one(email=data['email'])
    if not user:
        return response_fail(HTTPStatus.NOT_FOUND, 'User not registered')

    if user.password != data['password']:
        return response_fail(HTTPStatus.BAD_REQUEST, 'Email or password incorrect')

    user.token = crete_token()
    user.last_login = datetime.utcnow()
    try:
        db_save(user)
    except Exception as e:
        logger.error(f'{sys._getframe().f_code.co_name} {type(e).__name__}: {e}')
        return response_fail(HTTPStatus.INTERNAL_SERVER_ERROR, 'Internal server error')

    return response_ok('Successfully logged in', {'token': user.token})


def create(data):
    user = get_one(email=data['email'])
    if user:
        return response_fail(HTTPStatus.CONFLICT, 'Email already exists')

    user = User(
        email=data['email'],
        password=data['password'],
        currency_id=data['currency_id'],
        balance=data['balance']
    )
    try:
        db_save(user)
    except Exception as e:
        logger.error(f'{sys._getframe().f_code.co_name} {type(e).__name__}: {e}')
        return response_fail(HTTPStatus.INTERNAL_SERVER_ERROR, 'Internal server error')

    return response_ok('Successfully created')


def get_one(id=None, token=None, email=None):
    """
    Получает информацию о пользователе
    :return User
    """
    if token:
        return User.query.filter_by(token=token).first()
    if id:
        return User.query.filter_by(id=id).first()
    if email:
        return User.query.filter(func.lower(User.email) == func.lower(email)).first()

    return None


def crete_token():
    return str(uuid.uuid4()).replace('-', '')


def token_is_valid(token):
    """
    Проверяет, валиден ли токен
    :return boolean
    """
    user = get_one(token=token)
    if user and user.token:
        if user.last_login >= datetime.utcnow() - timedelta(hours=AUTH_PERIOD):
            return True

    return False


def token_required(f):
    """
    Обертка для проверки авторизации
    """
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.headers.get('pwt')
        if not token_is_valid(token):
            abort(code=HTTPStatus.UNAUTHORIZED, message='User token is invalid')

        return f(*args, **kwargs)

    return decorated


def db_save(data):
    db.session.add(data)
    db.session.commit()
