import logging
from flask_restplus import Namespace, fields

logger = logging.getLogger(__name__)


class UserDto:
    api = Namespace('user', description='user related operations')
    user_login = api.model('user_login', {
        'email': fields.String(required=True, description='user email'),
        'password': fields.String(required=True, description='user password')
    })
    new_user = api.model('new_user', {
        'email': fields.String(required=True, description='user email'),
        'password': fields.String(required=True, description='user password'),
        'currency_id': fields.Integer(required=True, description='currency id'),
        'balance': fields.Fixed(required=True, description='money balance')
    })
    user_token = api.model('user_token', {
        'token': fields.String(description='user token')
    })


class TransferDto:
    api = Namespace('transfer', description='transfer operations')
    new_transfer = api.model('new_transfer', {
        'receiver_user_id': fields.Integer(required=True, description='receiver id'),
        'amount': fields.Fixed(required=True, description='money amount in current user currency')
    })
    transfer = api.model('transfer', {
        'date': fields.DateTime(description='transfer date'),
        'sender_user_id': fields.Integer(description='sender id'),
        'sender_currency_id': fields.Integer(description='sender currency id'),
        'sender_amount': fields.Fixed(description='sender money amount'),
        'receiver_user_id': fields.Integer(description='receiver id'),
        'receiver_currency_id': fields.Integer(description='receiver currency id'),
        'receiver_amount': fields.Fixed(description='receiver money amount')
    })
