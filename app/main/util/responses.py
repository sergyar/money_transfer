import logging
from http import HTTPStatus

logger = logging.getLogger(__name__)


def response_fail(http_code, message, params={}):
    """
    Получает картеж (response_object, http_code),
    где response_object содержит информацию о неуспешном выполнении
    :param http_code:
    :param message:
    :param params: не обязательное - dict of params
    :return: картеж (response_object, http_code)
    """
    response_object = {
        'status': 'fail',
        'message': message
    }
    if params:
        response_object.update(params)
    return response_object, http_code


def response_ok(message, params={}):
    """
    Получает картеж (response_object, http_code),
    где response_object содержит информацию об успешном выполнении
    :param message:
    :param params: не обязательное - dict of params
    :return: картеж (response_object, http_code)
    """
    response_object = {
        'status': 'success',
        'message': message
    }
    if params:
        response_object.update(params)
    return response_object, HTTPStatus.OK
