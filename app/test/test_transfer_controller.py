from unittest import TestCase, skip

from mock import patch

from app.main.controller.transfer_controller import TransferCreate, TransferList


@skip
class Test(TestCase):

    def test_transfer_create(self):
        token = 'token'
        with patch('app.main.service.user_service.create') as create:
            with patch('app.main.service.user_service.get_user') as get_user:
                with patch('app.main.controller.user_controller.request') as request:
                    request.headers.get('pwt').return_result = token
                    TransferCreate().post()
                    get_user.assert_called_with(token=token)
                    create.assert_called()

    def test_transfer_list(self):
        token = 'token'
        with patch('app.main.service.user_service.get_all') as get_all:
            with patch('app.main.service.user_service.get_user') as get_user:
                with patch('app.main.controller.user_controller.request') as request:
                    request.headers.get('pwt').return_result = token
                    TransferList().post()
                    get_user.assert_called_with(token=token)
                    get_all.assert_called()
