from http import HTTPStatus
from unittest import TestCase
from mock import patch, Mock

from app.main.service.transfer_service import create as transfer_create, get_all, MIN_AMOUNT
from app.main.model.user import User
from app.main.util.responses import response_ok, response_fail


class Test(TestCase):

    def test_transfer_create_ok(self):
        data = {
            'receiver_user_id': 2,
            'amount': 100
        }
        sender = User(
            id=1,
            currency_id=1,
            balance=100
        )
        receiver = User(
            id=2,
            currency_id=1,
            balance=100
        )
        sender_amount = float(data['amount'])
        receiver_amount = sender_amount + 1
        new_sender_balance = sender.balance - sender_amount
        new_receiver_balance = receiver.balance + receiver_amount
        with patch('app.main.service.transfer_service.get_user', return_value=receiver) as get_user:
            with patch('app.main.service.transfer_service.convert_currency', return_value=receiver_amount) as convert_currency:
                with patch('app.main.service.transfer_service.db_save') as db_save:
                    self.assertEqual(transfer_create(sender, data), response_ok('Successfully created'))
                    get_user.assert_called_with(id=data['receiver_user_id'])
                    convert_currency.assert_called_with(sender.currency_id, receiver.currency_id, sender_amount)
                    db_save.assert_called()
                    arg_sender = db_save.call_args[0][0]
                    arg_receiver = db_save.call_args[0][1]
                    arg_transfer = db_save.call_args[0][2]
                    self.assertTrue(sender.id == arg_sender.id and sender.id == arg_transfer.sender_user_id)
                    self.assertTrue(receiver.id == arg_receiver.id and receiver.id == arg_transfer.receiver_user_id)
                    self.assertEqual(sender_amount, arg_transfer.sender_amount)
                    self.assertEqual(receiver_amount, arg_transfer.receiver_amount)
                    self.assertEqual(arg_sender.balance, new_sender_balance)
                    self.assertEqual(arg_receiver.balance, new_receiver_balance)

    def test_transfer_error_sender_not_found(self):
        data = {
            'receiver_user_id': 2,
            'amount': 100
        }
        sender = None  # will raise error
        with patch('app.main.service.transfer_service.get_user') as get_user:
            with patch('app.main.service.transfer_service.convert_currency') as convert_currency:
                with patch('app.main.service.transfer_service.db_save') as db_save:
                    self.assertEqual(transfer_create(sender, data), response_fail(HTTPStatus.BAD_REQUEST, 'Sender user not found'))
                    get_user.assert_not_called()
                    convert_currency.assert_not_called()
                    db_save.assert_not_called()

    def test_transfer_error_receiver_not_found(self):
        data = {
            'receiver_user_id': 2,
            'amount': 100
        }
        sender = User(
            id=1,
            currency_id=1,
            balance=100
        )
        with patch('app.main.service.transfer_service.get_user', return_value=None) as get_user:  # will raise error
            with patch('app.main.service.transfer_service.convert_currency') as convert_currency:
                with patch('app.main.service.transfer_service.db_save') as db_save:
                    self.assertEqual(transfer_create(sender, data), response_fail(HTTPStatus.BAD_REQUEST, 'Receiver user not found'))
                    get_user.assert_called()
                    convert_currency.assert_not_called()
                    db_save.assert_not_called()

    def test_transfer_error_impossible_transfer(self):
        data = {
            'receiver_user_id': 1,  # will raise error
            'amount': 100
        }
        sender = User(
            id=1,
            currency_id=1,
            balance=100
        )
        receiver = User(
            id=1,
            currency_id=1,
            balance=100
        )
        with patch('app.main.service.transfer_service.get_user', return_value=receiver) as get_user:
            with patch('app.main.service.transfer_service.convert_currency') as convert_currency:
                with patch('app.main.service.transfer_service.db_save') as db_save:
                    self.assertEqual(transfer_create(sender, data), response_fail(HTTPStatus.BAD_REQUEST, 'Impossible transfer'))
                    get_user.assert_called()
                    convert_currency.assert_not_called()
                    db_save.assert_not_called()

    def test_transfer_error_transfer_amount_too_low(self):
        data = {
            'receiver_user_id': 2,
            'amount': 0.9 * MIN_AMOUNT  # will raise error
        }
        sender = User(
            id=1,
            currency_id=1,
            balance=100
        )
        receiver = User(
            id=2,
            currency_id=1,
            balance=100
        )
        with patch('app.main.service.transfer_service.get_user', return_value=receiver) as get_user:
            with patch('app.main.service.transfer_service.convert_currency') as convert_currency:
                with patch('app.main.service.transfer_service.db_save') as db_save:
                    self.assertEqual(transfer_create(sender, data), response_fail(HTTPStatus.BAD_REQUEST, 'Transfer amount is too low'))
                    get_user.assert_called()
                    convert_currency.assert_not_called()
                    db_save.assert_not_called()

    def test_transfer_error_balance_too_low(self):
        data = {
            'receiver_user_id': 2,
            'amount': 100
        }
        sender = User(
            id=1,
            currency_id=1,
            balance=50  # will raise error
        )
        receiver = User(
            id=2,
            currency_id=1,
            balance=100
        )
        with patch('app.main.service.transfer_service.get_user', return_value=receiver) as get_user:
            with patch('app.main.service.transfer_service.convert_currency') as convert_currency:
                with patch('app.main.service.transfer_service.db_save') as db_save:
                    self.assertEqual(transfer_create(sender, data), response_fail(HTTPStatus.BAD_REQUEST, 'Sender balance is too low'))
                    get_user.assert_called()
                    convert_currency.assert_not_called()
                    db_save.assert_not_called()

    def test_transfer_error_convertation(self):
        data = {
            'receiver_user_id': 2,
            'amount': 100
        }
        sender = User(
            id=1,
            currency_id=1,
            balance=100
        )
        receiver = User(
            id=2,
            currency_id=1,
            balance=100
        )
        with patch('app.main.service.transfer_service.get_user', return_value=receiver) as get_user:
            with patch('app.main.service.transfer_service.convert_currency', return_value=None) as convert_currency:  # will raise error
                with patch('app.main.service.transfer_service.db_save') as db_save:
                    self.assertEqual(transfer_create(sender, data), response_fail(HTTPStatus.INTERNAL_SERVER_ERROR, 'Internal server error'))
                    get_user.assert_called()
                    convert_currency.assert_called()
                    db_save.assert_not_called()

    def test_transfer_error_db_save(self):
        data = {
            'receiver_user_id': 2,
            'amount': 100
        }
        sender = User(
            id=1,
            currency_id=1,
            balance=100
        )
        receiver = User(
            id=2,
            currency_id=1,
            balance=100
        )
        with patch('app.main.service.transfer_service.get_user', return_value=receiver) as get_user:
            with patch('app.main.service.transfer_service.convert_currency', return_value=data['amount']) as convert_currency:
                with patch('app.main.service.transfer_service.db_save', side_effect=Exception()) as db_save:  # will raise error
                    self.assertEqual(transfer_create(sender, data), response_fail(HTTPStatus.INTERNAL_SERVER_ERROR, 'Internal server error'))
                    get_user.assert_called()
                    convert_currency.assert_called()
                    db_save.assert_called()

    def test_transfer_get_all_ok(self):
        user = User(
            id=1
        )
        with patch('app.main.service.transfer_service.Transfer') as model_mock:
            expected = [1, 2, 3]
            all = model_mock.query.filter_by.return_value.all
            all.return_value = expected
            self.assertEqual(get_all(user), expected + expected)
            self.assertEqual(all.call_count, 2)
