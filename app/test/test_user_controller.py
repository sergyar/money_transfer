from unittest import TestCase, skip
from mock import patch

from app.main.controller.user_controller import UserCreate, UserLogin


@skip
class Test(TestCase):

    def test_user_create(self):
        data = {
            'email': 'email',
            'password': 'password',
            'currency_id': 1,
            'balance': 100
        }
        with patch('app.main.service.user_service.create', return_value=None) as create:
            with patch('app.main.controller.user_controller.request', json=data):
                UserCreate().post()
                create.assert_called_with(data)

    def test_login(self):
        data = {
            'email': 'email',
            'password': 'password'
        }
        with patch('app.main.service.user_service.login', return_value=None) as login:
            with patch('app.main.controller.user_controller.request', json=data):
                UserLogin().post()
                login.assert_called_with(data)
