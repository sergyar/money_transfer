from http import HTTPStatus
from unittest import TestCase
from mock import patch

from app.main.service.user_service import create as user_create, login, get_one
from app.main.model.user import User
from app.main.util.responses import response_ok, response_fail


class Test(TestCase):

    def test_user_create_ok(self):
        data = {
            'email': 'email',
            'password': 'password',
            'currency_id': 1,
            'balance': 100
        }
        user = User(
            email=data['email'],
            password=data['password'],
            currency_id=data['currency_id'],
            balance=data['balance']
        )
        with patch('app.main.service.user_service.get_one', return_value=None) as get_one:
            with patch('app.main.service.user_service.db_save') as db_save:
                self.assertEqual(user_create(data), response_ok('Successfully created'))
                get_one.assert_called_with(email=data['email'])
                db_save.assert_called()
                arg_user = db_save.call_args[0][0]
                self.assertEqual(user.email, arg_user.email)

    def test_user_create_already_exists_error(self):
        data = {
            'email': 'email',
            'password': 'password',
            'currency_id': 1,
            'balance': 100
        }
        user = User(
            email=data['email'],
            password=data['password'],
            currency_id=data['currency_id'],
            balance=data['balance']
        )
        with patch('app.main.service.user_service.get_one', return_value=user) as get_one:  # will raise error
            with patch('app.main.service.user_service.db_save') as db_save:
                self.assertEqual(user_create(data), response_fail(HTTPStatus.CONFLICT, 'Email already exists'))
                get_one.assert_called()
                db_save.assert_not_called()

    def test_user_create_db_save_error(self):
        data = {
            'email': 'email',
            'password': 'password',
            'currency_id': 1,
            'balance': 100
        }
        with patch('app.main.service.user_service.get_one', return_value=None) as get_one:
            with patch('app.main.service.user_service.db_save', side_effect=Exception()) as db_save:  # will raise error
                self.assertEqual(user_create(data), response_fail(HTTPStatus.INTERNAL_SERVER_ERROR, 'Internal server error'))
                get_one.assert_called()
                db_save.assert_called()

    def test_user_login_ok(self):
        data = {
            'email': 'email',
            'password': 'password'
        }
        user = User(
            email=data['email'],
            password=data['password'],
            token='token'
        )
        with patch('app.main.service.user_service.get_one', return_value=user) as get_one:
            with patch('app.main.service.user_service.crete_token', return_value=user.token) as create_token:
                with patch('app.main.service.user_service.db_save') as db_save:
                    self.assertEqual(login(data), response_ok('Successfully logged in', {'token': user.token}))
                    get_one.assert_called_with(email=data['email'])
                    create_token.assert_called()
                    db_save.assert_called()
                    arg_user = db_save.call_args[0][0]
                    self.assertEqual(user.email, arg_user.email)

    def test_user_login_error_not_registered(self):
        data = {
            'email': 'email',
            'password': 'password'
        }
        with patch('app.main.service.user_service.get_one', return_value=None) as get_one:  # will raise error
            with patch('app.main.service.user_service.crete_token') as create_token:
                with patch('app.main.service.user_service.db_save') as db_save:
                    self.assertEqual(login(data), response_fail(HTTPStatus.NOT_FOUND, 'User not registered'))
                    get_one.assert_called()
                    create_token.assert_not_called()
                    db_save.assert_not_called()

    def test_user_login_error_incorrect_password(self):
        data = {
            'email': 'email',
            'password': 'password'
        }
        user = User(
            email=data['email'],
            password='other password'  # will raise error
        )
        with patch('app.main.service.user_service.get_one', return_value=user) as get_one:
            with patch('app.main.service.user_service.crete_token') as create_token:
                with patch('app.main.service.user_service.db_save') as db_save:
                    self.assertEqual(login(data), response_fail(HTTPStatus.BAD_REQUEST, 'Email or password incorrect'))
                    get_one.assert_called()
                    create_token.assert_not_called()
                    db_save.assert_not_called()

    def test_user_login_error_db_save(self):
        data = {
            'email': 'email',
            'password': 'password'
        }
        user = User(
            email=data['email'],
            password=data['password'],
            token='token'
        )
        with patch('app.main.service.user_service.get_one', return_value=user) as get_one:
            with patch('app.main.service.user_service.crete_token', return_value=user.token) as create_token:
                with patch('app.main.service.user_service.db_save', side_effect=Exception()) as db_save:  # will raise error
                    self.assertEqual(login(data), response_fail(HTTPStatus.INTERNAL_SERVER_ERROR, 'Internal server error'))
                    get_one.assert_called()
                    create_token.assert_called()
                    db_save.assert_called()

    def test_user_get_one_by_id_ok(self):
        user = User(
            id=1
        )
        with patch('app.main.service.user_service.User') as model_mock:
            first = model_mock.query.filter_by.return_value.first
            first.return_value = user
            self.assertEqual(get_one(id=user.id).id, user.id)
            first.assert_called_once()

    def test_user_get_one_by_id_returns_none(self):
        with patch('app.main.service.user_service.User') as model_mock:
            first = model_mock.query.filter_by.return_value.first
            first.return_value = None
            self.assertIsNone(get_one(id=1))
            first.assert_called_once()

    def test_user_get_one_by_token_ok(self):
        user = User(
            token='token'
        )
        with patch('app.main.service.user_service.User') as model_mock:
            first = model_mock.query.filter_by.return_value.first
            first.return_value = user
            self.assertEqual(get_one(token=user.token).token, user.token)
            first.assert_called_once()

    def test_user_get_one_by_token_returns_none(self):
        with patch('app.main.service.user_service.User') as model_mock:
            first = model_mock.query.filter_by.return_value.first
            first.return_value = None
            self.assertIsNone(get_one(token='token'))
            first.assert_called_once()

    def test_user_get_one_by_email_ok(self):
        user = User(
            email='email'
        )
        with patch('app.main.service.user_service.User') as model_mock:
            first = model_mock.query.filter.return_value.first
            first.return_value = user
            self.assertEqual(get_one(email=user.email).email, user.email)
            first.assert_called_once()

    def test_user_get_one_by_email_returns_none(self):
        with patch('app.main.service.user_service.User') as model_mock:
            first = model_mock.query.filter.return_value.first
            first.return_value = None
            self.assertIsNone(get_one(email='email'))
            first.assert_called_once()
