#!/bin/bash

source venv/bin/activate
python money_transfer.py db migrate
python money_transfer.py db upgrade
python money_transfer.py run
