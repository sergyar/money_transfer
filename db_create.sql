DROP DATABASE IF EXISTS money_transfer;
DROP USER IF EXISTS money_transfer;

CREATE USER money_transfer WITH password 'money_transfer';
CREATE DATABASE money_transfer;
GRANT ALL ON DATABASE money_transfer TO money_transfer;
