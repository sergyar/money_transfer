# coding=utf-8
import logging.config
import logging.handlers
from threading import Timer

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from app import blueprint
from app.main import create_app, db
from app.main.config import cfg

logging.config.fileConfig(fname=cfg['local']['logger_config'], disable_existing_loggers=False)
logger = logging.getLogger(__name__)

app = create_app()
app.register_blueprint(blueprint)
app.app_context().push()

manager = Manager(app)

migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)


def refresh_currency_timer():
    """
    Оработчик таймера обновления валют
    :return:
    """
    logger.info('Refresh currency started')
    from app.main.service.currency_service import refresh_rates
    with app.app_context():
        refresh_rates()
    Timer(60*int(cfg['local']['refresh_currency_time']), refresh_currency_timer).start()


@manager.command
def run():
    Timer(1, refresh_currency_timer).start()
    app.run(host='0.0.0.0', port=cfg['local']['port'])


@manager.command
def db_init():
    print('db_init ... ')


if __name__ == '__main__':
    manager.run()
